usage: check_iptorrents [-h] [-w WARNING] [-c CRITICAL] [-u USERNAME]
                        [-p PASSWORD] [-t TIMEOUT]

Check IPTorrents rating.

optional arguments:
  -h, --help            show this help message and exit
  -w WARNING, --warning WARNING
                        Ratio which generates a warning state (default: 1.05)
  -c CRITICAL, --critical CRITICAL
                        Ratio which generates a critical state (default: 0.96)
  -u USERNAME, --username USERNAME
                        IPTorrents username (default: None)
  -p PASSWORD, --password PASSWORD
                        IPTorrents password (default: None)
  -t TIMEOUT, --timeout TIMEOUT
                        Seconds before check timeout (default: 10)
